# Simple key/value storage

Use {"id":"some_id"}

```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"test":"Hello world","id":"test"}' \
  https://54abrqnwi5.execute-api.eu-central-1.amazonaws.com/dev
```

https://54abrqnwi5.execute-api.eu-central-1.amazonaws.com/dev
